﻿
/**
 * Module dependencies.
 */

var fs = require('fs');
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function () {
    console.log('Math App server listening on port ' + app.get('port'));
});

app.get('/listUsers', function (req, res) {
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        console.log(data);
        res.end(data);
    });
})

app.get('/add', function (req, res) {
    var num1 = parseInt(req.param('num1'));
    var num2 = parseInt(req.param('num2'));
    if (!isNaN(num1) && !isNaN(num2)) {
        //var data = req.param('data');
        res.send((num1 + num2).toString());
    }
    else {
        res.send('send valid numbers.');
    }
})

var Client = require('node-rest-client').Client;
var client = new Client();


app.get('/addFromWcf', function (req, res) {
    // direct way 
    client.get("http://localhost:8733/MathSvc/json/Add?num1=5&num2=2", function (data, response) {
        // parsed response body as js object 
        console.log(data);
        // raw response 
        console.log(response);
        res.send(data);
    });
})

app.get('/specialaddFromWcf', function (req, res) {
    // direct way 
    client.get("http://localhost:8733/MathSvc/json/SpecialAdd?num1=5&num2=2", function (data, response) {
        // parsed response body as js object 
        console.log(data);
        // raw response 
        console.log(response);
        res.send(data);
    });
})